﻿using Credit;
using Credit.Calculateurs;
using Credit.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CreditTests
{
    public class CalculateurGeneralDonnees
    {
        public static IEnumerable<object[]> DonneesMensualiteGlobale
        {
            get
            {
                yield return new object[] { 783.0, new Emprunt(175000, 1.27, 25 * 12), 0.7};
                yield return new object[] { 1240.0, new Emprunt(200000, 0.73, 15 * 12), 0.4 };
            }
        }

        public static IEnumerable<object[]> DonneesMensualiteAssurance
        {
            get
            {
                yield return new object[] { 102.0, new Emprunt(175000, 1.27, 25 * 12), 0.7 };
                yield return new object[] { 67.0, new Emprunt(200000, 0.73, 15 * 12), 0.4 };
            }
        }

        public static IEnumerable<object[]> DonneesMontantTotalInterets
        {
            get
            {
                yield return new object[] { 29341, new Emprunt(175000, 1.27, 25 * 12) };
                yield return new object[] { 11211, new Emprunt(200000, 0.73, 15 * 12) };
            }
        }

        public static IEnumerable<object[]> DonneesMontantTotalAssurance
        {
            get
            {
                yield return new object[] { 30625, new Emprunt(175000, 1.27, 25 * 12), 0.7 };
                yield return new object[] { 12000, new Emprunt(200000, 0.73, 15 * 12), 0.4 };
            }
        }
        
        public static IEnumerable<object[]> DonneesCalculerCapitalRembourse
        {
            get
            {
                yield return new object[] { 496, new Emprunt(175000, 1.27, 25 * 12), 1 };
                yield return new object[] { 175000, new Emprunt(175000, 1.27, 25 * 12), 300 };
            }
        }
    }

    public class CalculateurGeneralTests
    {
        [Theory]
        [MemberData(nameof(CalculateurGeneralDonnees.DonneesMensualiteGlobale), MemberType = typeof(CalculateurGeneralDonnees))]
        public void CalculerMensualiteGlobale(double mensualiteAttendue, Emprunt emprunt, double tauxAssurance)
        {
            Assert.Equal(mensualiteAttendue, CalculateurGeneral.CalculerMensualiteGlobale(emprunt, tauxAssurance), 0);
        }

        [Theory]
        [MemberData(nameof(CalculateurGeneralDonnees.DonneesMensualiteAssurance), MemberType = typeof(CalculateurGeneralDonnees))]
        public void CalculerMensualiteAssurance(double mensualiteAttendue, Emprunt emprunt, double tauxAssurance)
        {
            var calculateurMensualite = new CalculateurMensualite();
            Assert.Equal(mensualiteAttendue, calculateurMensualite.CalculerMensualiteAssurance(emprunt, tauxAssurance), 0);
        }

        [Theory]
        [MemberData(nameof(CalculateurGeneralDonnees.DonneesMontantTotalInterets), MemberType = typeof(CalculateurGeneralDonnees))]
        public void CalculerMontantTotalInterets(double montantTotalAttendu, Emprunt emprunt)
        {
            Assert.Equal(montantTotalAttendu, CalculateurGeneral.CalculerMontantTotalInterets(emprunt), 0);
        }

        [Theory]
        [MemberData(nameof(CalculateurGeneralDonnees.DonneesMontantTotalAssurance), MemberType = typeof(CalculateurGeneralDonnees))]
        public void CalculerMontantTotalAssurance(double montantTotalAttendu, Emprunt emprunt, double tauxAssurance)
        {
            Assert.Equal(montantTotalAttendu, CalculateurGeneral.CalculerMontantTotalAssurance(emprunt, tauxAssurance), 0);
        }

        [Theory]
        [MemberData(nameof(CalculateurGeneralDonnees.DonneesCalculerCapitalRembourse), MemberType = typeof(CalculateurGeneralDonnees))]
        public void CalculerCapitalRembourse(double valeurAttendue, Emprunt emprunt, uint numeroMois)
        {
            Assert.Equal(valeurAttendue, CalculateurGeneral.CalculerCapitalRembourse(emprunt, numeroMois), 0);
        }
    }
}
