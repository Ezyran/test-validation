﻿using Credit.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CreditTests
{    
    public class CalculateurTauxAssuranceDonnees
    {
        public static IEnumerable<object[]> Donnees
        {
            get
            {
                yield return new object[] { 0.8f, new Client(true, true, true, true, true) };
                yield return new object[] { 0.3f, new Client(false, false, false, false, false) };
                yield return new object[] { 0.2f, new Client(true, false, false, true, false) };
                yield return new object[] { 0.7f, new Client(false, true, true, true, false) };
            }
        }
    }

    public class CalculateurTauxAssuranceTests
    {
        [Fact]
        public void CreationCalculateurTauxAssurance()
        {
            var calculateurAssurance = new CalculateurTauxAssurance();
        }

        [Theory]
        [MemberData(nameof(CalculateurTauxAssuranceDonnees.Donnees), MemberType = typeof(CalculateurTauxAssuranceDonnees))]
        public void CalculerTauxAssurance(double tauxAttendu, Client client)
        {
            var calculateurAssurance = new CalculateurTauxAssurance();
            double tauxAssurance = calculateurAssurance.CalculerTauxAssurance(client);
            Assert.Equal(tauxAttendu, tauxAssurance, 2);
        }
    }
}
