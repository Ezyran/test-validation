﻿using Credit.Entities;
using Credit.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CreditTests
{
    public class EmpruntTests
    {
        [Theory]
        [InlineData(0)]
        [InlineData(50000)]
        public void RenvoieMontantInvalideException(uint capitalEmprunte)
        {
            Assert.Throws<MontantInvalideException>(() => new Emprunt(capitalEmprunte, 1.27, 300));
        }

        [Theory]
        [InlineData(0.0)]
        [InlineData(-10.0)]
        [InlineData(101.0)]
        public void RenvoieTauxInvalideException(double tauxAnnuel)
        {
            Assert.Throws<TauxInvalideException>(() => new Emprunt(200000, tauxAnnuel, 300));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(107)]
        [InlineData(301)]
        public void RenvoieDureeInvalideException(uint dureeMois)
        {
            Assert.Throws<DureeInvalideException>(() => new Emprunt(200000, 1.27, dureeMois));
        }
    }
}
