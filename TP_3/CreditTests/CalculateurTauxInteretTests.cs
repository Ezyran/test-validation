﻿using Credit.DataTypes;
using Credit.Entities;
using Credit.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CreditTests
{
    public class FabriqueTauxInteretTests
    {
        [Theory]
        [InlineData(0.62, 7, ETypeTaux.Bon)]
        [InlineData(0.85, 15, ETypeTaux.Bon)]
        [InlineData(0.73, 15, ETypeTaux.TresBon)]
        [InlineData(0.89, 25, ETypeTaux.Excellent)]
        public void CalculerTaux(double tauxAttendu, uint dureeAnnees, ETypeTaux typeTaux)
        {
            TauxInteret taux = FabriqueTauxInteret.CreerTauxInteret(dureeAnnees, typeTaux);
            Assert.Equal(tauxAttendu, taux.Taux);
        }
    }
}
