﻿using Credit.Calculateurs;
using Credit.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CreditTests
{
    public class CalculateurAmortissementDonnees
    {
        public static IEnumerable<object[]> DonneesCalculerRestantDuPremierMois
        {
            get
            {
                yield return new object[] { 174504, new Emprunt(175000, 1.27, 25 * 12) };
            }
        }

        public static IEnumerable<object[]> DonneesCreerLigneAmortissement
        {
            get
            {
                yield return new object[] { new LigneAmortissement(1, 174504, 185, 496), new Emprunt(175000, 1.27, 25 * 12), 1 };
                yield return new object[] { new LigneAmortissement(300, 0, 1, 680), new Emprunt(175000, 1.27, 25 * 12), 300 };
            }
        }
    }
    public class CalculateurAmortissementTests
    {
        [Theory]
        [MemberData(nameof(CalculateurAmortissementDonnees.DonneesCalculerRestantDuPremierMois), MemberType = typeof(CalculateurAmortissementDonnees))]
        public void CalculerRestantDuPremierMois(double restantDuAttendu, Emprunt emprunt)
        {
            Assert.Equal(restantDuAttendu, CalculateurAmortissement.CalculerRestantDuPremierMois(emprunt), 0);
        }

        [Theory]
        [MemberData(nameof(CalculateurAmortissementDonnees.DonneesCreerLigneAmortissement), MemberType = typeof(CalculateurAmortissementDonnees))]
        public void CreerLigneAmortissement(LigneAmortissement ligneAmortissementAttendue, Emprunt emprunt, uint numeroMois)
        {
            var ligneAmortissement = CalculateurAmortissement.CreerLigneAmortissement(emprunt, numeroMois);
            Assert.Equal(ligneAmortissementAttendue.ResteAPayer, ligneAmortissement.ResteAPayer, 0);
            Assert.Equal(ligneAmortissementAttendue.PartInteret, ligneAmortissement.PartInteret, 0);
            Assert.Equal(ligneAmortissementAttendue.PartPret, ligneAmortissement.PartPret, 0);
        }
    }

}
