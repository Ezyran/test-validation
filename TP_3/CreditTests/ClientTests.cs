﻿using Credit.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CreditTests
{
    public class ClientTests
    {
        [Theory]
        [InlineData(true, true, true, true, true)]
        [InlineData(false, false, false, false, false)]
        public void CreationClient(bool estSportif, bool estFumeur, bool estCardiaque, bool estIngenieurInformatique, bool estPiloteChasse)
        {
            var client = new Client(estSportif, estFumeur, estCardiaque, estIngenieurInformatique, estPiloteChasse);
        }
    }
}
