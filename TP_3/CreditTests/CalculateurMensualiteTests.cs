﻿using Credit;
using Credit.Calculateurs;
using Credit.Entities;
using Credit.Exceptions;
using Xunit;

namespace CreditTests
{

    public class CalculateurMensualiteTests
    {
        [Fact]
        public void CreationCalculateur()
        {
            var calculateur = new CalculateurMensualite();
        }

        [Theory]
        [InlineData(175000, 1.27, 300)]
        [InlineData(200000, 0.73, 180)]
        public void CreationEmprunt(uint capitalEmprunte, double tauxAnnuel, uint dureeMois)
        {
            var credit = new Emprunt(capitalEmprunte, tauxAnnuel, dureeMois);
        }

        [Theory]
        [InlineData(681, 175000, 1.27, 300)]
        [InlineData(1173, 200000, 0.73, 180)]
        public void CalculerMensualite(double mensualiteAttendue, uint capitalEmprunte, double tauxAnnuel, uint dureeMois)
        {
            var emprunt = new Emprunt(capitalEmprunte, tauxAnnuel, dureeMois);
            var calculateur = new CalculateurMensualite();
            double mensualite = calculateur.CalculerMensualiteInteret(emprunt);
            Assert.Equal(mensualiteAttendue, mensualite, 0);
        }
    }
}
