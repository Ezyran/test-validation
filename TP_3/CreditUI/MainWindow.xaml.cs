﻿using Credit;
using Credit.Calculateurs;
using Credit.DataTypes;
using Credit.Entities;
using Credit.Enums;
using System;
using System.Windows;
using System.Windows.Controls;

namespace CreditUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCalculer_Click(object sender, RoutedEventArgs e)
        {
            var calculateurAssurance = new CalculateurTauxAssurance();
            Emprunt emprunt = CreerEmprunt();
            Client client = CreerClient();
            double tauxAssurance = calculateurAssurance.CalculerTauxAssurance(client);
            AfficherDetailEmprunt(emprunt, tauxAssurance);
        }

        private Client CreerClient()
        {
            bool estFumeur = (ckbFumeur.IsChecked == true);
            bool estSportif = (ckbSportif.IsChecked == true);
            bool estCardiaque = (ckbCardiaque.IsChecked == true);
            bool estIngenieurInfo = (ckbIngenieurInfo.IsChecked == true);
            bool estPiloteChasse = (ckbPiloteChasse.IsChecked == true);

            Client client = new Client(estSportif, estFumeur, estCardiaque, estIngenieurInfo, estPiloteChasse);
            return client;
        }

        private Emprunt CreerEmprunt()
        {
            string nomTypeTaux = ((ComboBoxItem)lstTypeTaux.SelectedItem).Content.ToString();
            ETypeTaux typeTaux = ETypeTaux.Bon;
            switch (nomTypeTaux)
            {
                case "Bon taux":
                    typeTaux = ETypeTaux.Bon;
                    break;
                case "Très bon taux":
                    typeTaux = ETypeTaux.TresBon;
                    break;
                case "Excellent taux":
                    typeTaux = ETypeTaux.Excellent;
                    break;
            }
            uint valeurDuree = UInt32.Parse(txtDuree.Text);
            uint valeurMontant = UInt32.Parse(txtMontant.Text);
            TauxInteret tauxInteret = FabriqueTauxInteret.CreerTauxInteret(valeurDuree, typeTaux);

            return new Emprunt(valeurMontant, tauxInteret.Taux, valeurDuree*12);
        }

        private void AfficherDetailEmprunt(Emprunt emprunt, double tauxAssurance)
        {
            var calculateurMensualite = new CalculateurMensualite();
            outputCoutMensuel.Content = Math.Round(CalculateurGeneral.CalculerMensualiteGlobale(emprunt, tauxAssurance));
            outputAssuranceMensuelle.Content = Math.Round(calculateurMensualite.CalculerMensualiteAssurance(emprunt, tauxAssurance));
            outputCoutTotalInterets.Content = Math.Round(CalculateurGeneral.CalculerMontantTotalInterets(emprunt));
            outputCoutTotalAssurance.Content = Math.Round(CalculateurGeneral.CalculerMontantTotalAssurance(emprunt, tauxAssurance));

            if (emprunt.Duree.DureeMois < 120)
                outputMontantRembourseApres10ans.Content = emprunt.CapitalEmprunte.Montant;
            else
                outputMontantRembourseApres10ans.Content = Math.Round(CalculateurGeneral.CalculerCapitalRembourse(emprunt, 120));
        }
    }
}
