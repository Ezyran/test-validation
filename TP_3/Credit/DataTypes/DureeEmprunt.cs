﻿using Credit.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.DataTypes
{
    public struct DureeEmprunt
    {
        private static readonly uint DUREE_MINIMUM_MOIS = 108;
        private static readonly uint DUREE_MAXIMUM_MOIS = 300;

        private uint _dureeMois = 0;

        public uint DureeMois
        {
            get => _dureeMois;
            set
            {
                if (value < DUREE_MINIMUM_MOIS || value > DUREE_MAXIMUM_MOIS)
                    throw new DureeInvalideException();
                _dureeMois = value;
            }
        }

        public DureeEmprunt(uint dureeMois)
        {
            DureeMois = dureeMois;
        }
    }
}
