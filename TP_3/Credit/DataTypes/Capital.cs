﻿using Credit.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.DataTypes
{
    public struct Capital
    {
        private static readonly uint MONTANT_MINIMUM = 50000;

        private uint _montant = 0;

        public uint Montant
        {
            get => _montant;
            set
            {
                if (value <= MONTANT_MINIMUM)
                    throw new MontantInvalideException();
                _montant = value;
            }
        }

        public Capital(uint montant)
        {
            Montant = montant;
        }

    }
}
