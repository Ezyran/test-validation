﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Entities
{
    public class LigneAmortissement
    {
        public uint NumeroMois { get; set; }
        public double ResteAPayer { get; set; }
        public double PartInteret { get; set; }
        public double PartPret { get; set; }

        public LigneAmortissement(uint numeroMois, double resteAPayer, double partInteret, double partPret)
        {
            NumeroMois = numeroMois;
            ResteAPayer = resteAPayer;
            PartInteret = partInteret;  
            PartPret = partPret;
        }
    }
}
