﻿using Credit.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.DataTypes
{
    public struct TauxInteret
    {
        private static readonly double TAUX_MINIMUM = 0.0;
        private static readonly double TAUX_MAXIMUM = 100.0;

        private double _taux = 0.0;

        public double Taux
        {
            get => _taux;
            set
            {
                if (value <= TAUX_MINIMUM || value >= TAUX_MAXIMUM)
                    throw new TauxInvalideException();
                _taux = value;
            }
        }

        public TauxInteret(double taux)
        {
            Taux = taux;
        }
    }
}
