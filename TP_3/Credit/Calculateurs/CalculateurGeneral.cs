﻿using Credit.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Calculateurs
{
    public static class CalculateurGeneral
    {

        public static double CalculerMensualiteGlobale(Emprunt emprunt, double tauxAssurance)
        {
            var calculateurMensualite = new CalculateurMensualite();
            double mensualiteInteret = calculateurMensualite.CalculerMensualiteInteret(emprunt);
            double mensualiteAssurance = calculateurMensualite.CalculerMensualiteAssurance(emprunt, tauxAssurance);
            return mensualiteInteret + mensualiteAssurance;
        }

        public static double CalculerMontantTotalInterets(Emprunt emprunt)
        {
            var calculateurMensualite = new CalculateurMensualite();
            double mensualiteInteret = calculateurMensualite.CalculerMensualiteInteret(emprunt);
            double montantTotalPaye = mensualiteInteret * emprunt.Duree.DureeMois;
            double montantTotalInterets = montantTotalPaye - emprunt.CapitalEmprunte.Montant;
            return montantTotalInterets;
        }

        public static double CalculerMontantTotalAssurance(Emprunt emprunt, double tauxAssurance)
        {
            var calculateurMensualite = new CalculateurMensualite();
            double mensualiteAssurance = calculateurMensualite.CalculerMensualiteAssurance(emprunt, tauxAssurance);
            return mensualiteAssurance * emprunt.Duree.DureeMois;
        }

        public static double CalculerCapitalRembourse(Emprunt emprunt, uint numeroMois)
        {
            var ligneAmortissementMoisCible = CalculateurAmortissement.CreerLigneAmortissement(emprunt, numeroMois);
            return emprunt.CapitalEmprunte.Montant - ligneAmortissementMoisCible.ResteAPayer;
        }
    }
}
