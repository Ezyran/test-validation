﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Entities
{
    public class CalculateurTauxAssurance
    {
        public CalculateurTauxAssurance()
        { }

        public double CalculerTauxAssurance(Client client)
        {
            double taux = 0.3;
            if (client.EstFumeur)
                taux += 0.15;
            if (client.EstSportif)
                taux -= 0.05;
            if (client.EstCardiaque)
                taux += 0.3;
            if (client.EstIngenieurInformatique)
                taux -= 0.05;
            if (client.EstPiloteChasse)
                taux += 0.15;
            return taux;
        }
    }
}
