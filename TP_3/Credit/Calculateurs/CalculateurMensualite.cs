﻿using Credit.Entities;

namespace Credit.Calculateurs
{
    public class CalculateurMensualite
    {
        private static uint MOIS_DANS_ANNEE = 12;

        public CalculateurMensualite() {}

        public double CalculerMensualiteInteret(Emprunt emprunt)
        {
            double numerateur = emprunt.CapitalEmprunte.Montant * (emprunt.TauxInteret.Taux / 100 / MOIS_DANS_ANNEE);
            double denominateur = 1 - Math.Pow(1 + (emprunt.TauxInteret.Taux / 100 / MOIS_DANS_ANNEE), -emprunt.Duree.DureeMois);

            return numerateur / denominateur;
        }

        public double CalculerMensualiteAssurance(Emprunt emprunt, double tauxAssurance)
        {
            return emprunt.CapitalEmprunte.Montant * (tauxAssurance / 100) / MOIS_DANS_ANNEE;
        }

        
    }
}
