﻿using Credit.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Calculateurs
{
    public static class CalculateurAmortissement
    {
        private static double CalculerPartInteretPremierMois(Emprunt emprunt)
        {
            var capitalEmprunte = emprunt.CapitalEmprunte.Montant;
            var tauxInteretAnnuel = emprunt.TauxInteret.Taux / 12;

            return (capitalEmprunte * tauxInteretAnnuel) / 100;
        }

        private static double CalculerCapitalAmortiPremierMois(Emprunt emprunt)
        {
            var calculateurMensualite = new CalculateurMensualite();
            double mensualite = calculateurMensualite.CalculerMensualiteInteret(emprunt);
            double partInteret = CalculerPartInteretPremierMois(emprunt);
            double capitalAmorti = mensualite - partInteret;
            return capitalAmorti;
        }

        public static double CalculerRestantDuPremierMois(Emprunt emprunt)
        {
            var calculateurMensualite = new CalculateurMensualite();
            return emprunt.CapitalEmprunte.Montant - CalculerCapitalAmortiPremierMois(emprunt);
        }

        public static LigneAmortissement CreerLigneAmortissement(Emprunt emprunt, uint numeroMois)
        {
            var calculateurMensualite = new CalculateurMensualite();
            double mensualite = calculateurMensualite.CalculerMensualiteInteret(emprunt);
            double tauxInteretAnnuel = emprunt.TauxInteret.Taux / 12;

            double partInteret = CalculerPartInteretPremierMois(emprunt);
            double partPret = mensualite - partInteret;
            double capitalAmorti = CalculerCapitalAmortiPremierMois(emprunt);
            double resteAPayer = emprunt.CapitalEmprunte.Montant - capitalAmorti;

            var ligneAmortissement = new LigneAmortissement(1, resteAPayer, partInteret, partPret);
            for (uint moisEnCours = 2; moisEnCours <= numeroMois; moisEnCours++)
            {
                ligneAmortissement.PartInteret = (ligneAmortissement.ResteAPayer * tauxInteretAnnuel) / 100;
                ligneAmortissement.PartPret = mensualite - ligneAmortissement.PartInteret;
                ligneAmortissement.ResteAPayer = ligneAmortissement.ResteAPayer - ligneAmortissement.PartPret;
            }
            return ligneAmortissement;
        }
    }
}
