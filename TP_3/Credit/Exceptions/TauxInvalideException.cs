﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Exceptions
{
    public class TauxInvalideException : Exception
    {
        public TauxInvalideException() { }
        public TauxInvalideException(string message) : base(message) { }
    }
}
