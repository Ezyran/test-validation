﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Exceptions
{
    public class DureeInvalideException : Exception
    {
        public DureeInvalideException() { }
        public DureeInvalideException(string message) : base(message) { }
    }
}
