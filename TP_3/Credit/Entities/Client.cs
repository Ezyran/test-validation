﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Entities
{
    public class Client
    {
        public bool EstSportif { get; set; } = false;
        public bool EstFumeur { get; set; } = false;
        public bool EstCardiaque { get; set; } = false;
        public bool EstIngenieurInformatique { get; set; } = false;
        public bool EstPiloteChasse { get; set; } = false;

        public Client(bool estSportif, bool estFumeur, bool estCardiaque, bool estIngenieurInformatique, bool estPiloteChasse)
        {
            EstSportif = estSportif;
            EstFumeur = estFumeur;
            EstCardiaque = estCardiaque;
            EstIngenieurInformatique = estIngenieurInformatique;
            EstPiloteChasse = estPiloteChasse;
        }
    }
}
