﻿using Credit.DataTypes;
using Credit.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Entities
{
    public class Emprunt
    {
        public Capital CapitalEmprunte;
        public TauxInteret TauxInteret;
        public DureeEmprunt Duree;

        public Emprunt(uint capitalEmprunte, double tauxAnnuel, uint dureeMois)
        {            
            CapitalEmprunte = new Capital(capitalEmprunte);
            TauxInteret = new TauxInteret(tauxAnnuel);
            Duree = new DureeEmprunt(dureeMois);
        }
    }
}
