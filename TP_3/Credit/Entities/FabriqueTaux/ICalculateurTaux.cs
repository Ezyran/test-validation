﻿using Credit.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Entities
{
    public interface ICalculateurTaux
    {
        public TauxInteret CalculerTaux(uint dureeAnnees);
    }
}
