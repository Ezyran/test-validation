﻿using Credit.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Entities
{
    public class CalculateurExcellentTaux : ICalculateurTaux
    {
        public TauxInteret CalculerTaux(uint dureeAnnees)
        {
            if (dureeAnnees < 10)
                return new TauxInteret(0.35);
            if (dureeAnnees < 15)
                return new TauxInteret(0.45);
            if (dureeAnnees < 20)
                return new TauxInteret(0.58);
            if (dureeAnnees < 25)
                return new TauxInteret(0.73);
            return new TauxInteret(0.89);
        }
    }
}
