﻿using Credit.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Entities
{
    public class CalculateurBonTaux : ICalculateurTaux
    {
        public TauxInteret CalculerTaux(uint dureeAnnees)
        {
            if (dureeAnnees < 10)
                return new TauxInteret(0.62);
            if (dureeAnnees < 15)
                return new TauxInteret(0.67);
            if (dureeAnnees < 20)
                return new TauxInteret(0.85);
            if (dureeAnnees < 25)
                return new TauxInteret(1.04);
            return new TauxInteret(1.27);
        }
    }
}
