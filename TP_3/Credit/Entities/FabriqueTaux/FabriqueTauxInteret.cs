﻿using Credit.DataTypes;
using Credit.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Entities
{
    public static class FabriqueTauxInteret
    {

        public static TauxInteret CreerTauxInteret(uint dureeAnnees, ETypeTaux typeTaux)
        {
            ICalculateurTaux calculateur;
            switch(typeTaux)
            {
                case ETypeTaux.Bon:
                    calculateur = new CalculateurBonTaux();
                    break;
                case ETypeTaux.TresBon:
                    calculateur = new CalculateurTresBonTaux();
                    break;
                case ETypeTaux.Excellent:
                    calculateur = new CalculateurExcellentTaux();
                    break;
                default:
                    throw new ArgumentException("Type de taux pas pris en charge");
            }
            return calculateur.CalculerTaux(dureeAnnees);
        }
    }
}
