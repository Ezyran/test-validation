﻿using Credit.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credit.Entities
{
    public class CalculateurTresBonTaux : ICalculateurTaux
    {
        public TauxInteret CalculerTaux(uint dureeAnnees)
        {
            if (dureeAnnees < 10)
                return new TauxInteret(0.43);
            if (dureeAnnees < 15)
                return new TauxInteret(0.55);
            if (dureeAnnees < 20)
                return new TauxInteret(0.73);
            if (dureeAnnees < 25)
                return new TauxInteret(0.91);
            return new TauxInteret(1.15);
        }
    }
}
