using System;

namespace Credit.Exceptions
{
    public class InvalidInterestRateException : Exception
    {
        public InvalidInterestRateException() {}
        public InvalidInterestRateException(string pMessage) : base(pMessage) {}
    }
}