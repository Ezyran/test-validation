using System;

namespace Credit.Exceptions
{
    public class InvalidInsuranceRateException : Exception
    {
        public InvalidInsuranceRateException() {}
        public InvalidInsuranceRateException(string pMessage) : base(pMessage) {}
    }
}