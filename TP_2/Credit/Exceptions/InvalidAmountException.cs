using System;

namespace Credit.Exceptions
{
    public class InvalidAmountException : Exception
    {
        public InvalidAmountException() {}
        public InvalidAmountException(string pMessage) : base(pMessage) {}
    }
}