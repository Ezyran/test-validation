using System;

namespace Credit.Exceptions
{
    public class InvalidRateTypeException : Exception
    {
        public InvalidRateTypeException() {}
        public InvalidRateTypeException(string pMessage) : base(pMessage) {}
    }
}