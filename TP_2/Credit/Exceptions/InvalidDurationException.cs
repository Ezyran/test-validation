using System;

namespace Credit.Exceptions
{
    public class InvalidDurationException : Exception
    {
        public InvalidDurationException() {}
        
        public InvalidDurationException(string pMessage) : base(pMessage) {}
    }
}