using System;
using Credit.Exceptions;

namespace Credit.DataTypes
{
    public struct MortgageAmount : IAmount
    {
        private static readonly uint MINIMUM_AMOUNT = 50000;
        
        private uint _amount;
        
        public MortgageAmount(uint pAmount) : this()
        {
            Amount = pAmount;
        }

        public uint Amount
        {
            get => _amount;
            set
            {
                if (value < MINIMUM_AMOUNT)
                    throw new InvalidAmountException($"Given amount of {value} is lower than minimal value of {MINIMUM_AMOUNT}");
                _amount = value;
            }
        }
    }

}