using Credit.Exceptions;

namespace Credit.DataTypes
{
    public struct MortgageRate : IInterestRate
    {
        private static readonly float MINIMUM_INTEREST_RATE = 0.0f;
        private static readonly float MAXIMUM_INTEREST_RATE = 100.0f;
        
        private float _interestRate;

        public MortgageRate(float pInterestRate) : this()
        {
            InterestRate = pInterestRate;
        }

        public float InterestRate
        {
            get => _interestRate;
            set
            {
                if (value < MINIMUM_INTEREST_RATE)
                    throw new InvalidInterestRateException($"Given interest rate of {value} is lesser than minimal value of {MINIMUM_INTEREST_RATE}");
                else if (value > MAXIMUM_INTEREST_RATE)
                    throw new InvalidInterestRateException($"Given interest rate of {value} is greater than maximal value of {MAXIMUM_INTEREST_RATE}");
                _interestRate = value;
            }
        }
    }
}