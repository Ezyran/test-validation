using Credit.Exceptions;

namespace Credit.DataTypes
{
    public struct MortgageDuration : IDuration
    {
        private static readonly uint MINIMUM_DURATION_IN_YEARS = 9;
        private static readonly uint MAXIMUM_DURATION_IN_YEARS = 25;

        private uint _durationInYears;

        public MortgageDuration(uint pDurationInYears) : this()
        {
            DurationInYears = pDurationInYears;
        }

        public uint DurationInYears
        {
            get => _durationInYears;
            set
            {
                if (value < MINIMUM_DURATION_IN_YEARS)
                    throw new InvalidDurationException($"Given duration of {value} is lesser than minimal value of {MINIMUM_DURATION_IN_YEARS}");
                else if (value > MAXIMUM_DURATION_IN_YEARS)
                    throw new InvalidDurationException($"Given duration of {value} is greater than maximal value of {MAXIMUM_DURATION_IN_YEARS}");
                _durationInYears = value;
            }
        }

        public uint GetDurationInMonth()
        {
            return _durationInYears * 12;
        }
    }
}