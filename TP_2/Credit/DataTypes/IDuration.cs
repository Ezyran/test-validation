namespace Credit.DataTypes
{
    public interface IDuration
    {
        public uint DurationInYears { get; set; }
        public uint GetDurationInMonth();
    }
}