namespace Credit.DataTypes
{
    public interface IInsuranceRate
    {
        public float InsuranceRate { get; set; }
        public float GetBaseInsuranceRate();
    }
}