namespace Credit.DataTypes
{
    public interface IAmount
    {
        public uint Amount { get; set; }
    }
}