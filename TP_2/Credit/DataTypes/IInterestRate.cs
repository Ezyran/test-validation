namespace Credit.DataTypes
{
    public interface IInterestRate
    {
        public float InterestRate { get; set; }
    }
}