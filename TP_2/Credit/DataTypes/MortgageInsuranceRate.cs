using Credit.Exceptions;

namespace Credit.DataTypes
{
    public struct MortgageInsuranceRate : IInsuranceRate
    {
        private static readonly float MINIMUM_INSURANCE_RATE = 0.10f;
        private static readonly float MAXIMUM_INSURANCE_RATE = 0.90f;
        
        public static readonly float BASE_INSURANCE_RATE = 0.30f;

        private float _insuranceRate;

        public MortgageInsuranceRate(float pInsuranceRate) : this()
        {
            InsuranceRate = pInsuranceRate;
        }

        public float InsuranceRate
        {
            get => _insuranceRate;
            set
            {
                if (value < MINIMUM_INSURANCE_RATE)
                    throw new InvalidInsuranceRateException($"Given Insurance Rate of {value} is lesser than the minimal value of {MINIMUM_INSURANCE_RATE}");
                else if (value > MAXIMUM_INSURANCE_RATE)
                    throw new InvalidInsuranceRateException($"Given Insurance Rate of {value} is greater than the maximal value of {MAXIMUM_INSURANCE_RATE}");
                _insuranceRate = value;
            }
        }

        public float GetBaseInsuranceRate()
        {
            return BASE_INSURANCE_RATE;
        }
    }
}