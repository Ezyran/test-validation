using System;
using System.Collections.ObjectModel;
using Credit.DataTypes;
using Credit.Enums;
using Credit.Exceptions;

namespace Credit.Factories
{
    public static class InterestRateFactory
    {
        private static readonly Collection<uint> YEAR_THRESHOLDS = new Collection<uint> {7, 10, 15, 20, 25};

        private static readonly Collection<float> GOOD_INTEREST_RATES = new Collection<float>() {0.62f, 0.67f, 0.85f, 1.04f, 1.27f};
        private static readonly Collection<float> VERY_GOOD_INTEREST_RATES = new Collection<float>() {0.43f, 0.55f, 0.73f, 0.91f, 1.15f};
        private static readonly Collection<float> EXCELLENT_INTEREST_RATES = new Collection<float>() {0.35f, 0.45f, 0.58f, 0.73f, 0.89f};
        
        public static IInterestRate CreateMortgageInterestRate(ERateType pRateType, IDuration pLoanDuration)
        {
            Collection<float> currentInterestRates;
            
            switch (pRateType)
            {
                case ERateType.BonTaux:
                    currentInterestRates = new Collection<float>(GOOD_INTEREST_RATES);
                    break;
                case ERateType.TresBonTaux:
                    currentInterestRates = new Collection<float>(VERY_GOOD_INTEREST_RATES);
                    break;
                case ERateType.ExcellentTaux:
                    currentInterestRates = new Collection<float>(EXCELLENT_INTEREST_RATES);
                    break;
                default:
                    throw new InvalidRateTypeException("Specified RateType doesn't match any usable RateType for this method.");
            }

            int mortgageInterestRateIndex = GetInterestRateIndex(pLoanDuration.DurationInYears);
            
            float interestRate = MathF.Round(currentInterestRates[mortgageInterestRateIndex], 2);
            return new MortgageRate(interestRate);
        }

        private static int GetInterestRateIndex(uint pDurationInYears)
        {
            int interestRateIndex = 0;

            for (int yearThresholdIndex = 0; yearThresholdIndex < YEAR_THRESHOLDS.Count; yearThresholdIndex++)
            {
                if (YEAR_THRESHOLDS[yearThresholdIndex] <= pDurationInYears)
                    interestRateIndex = yearThresholdIndex;
            }

            return interestRateIndex;
        }
    }
}