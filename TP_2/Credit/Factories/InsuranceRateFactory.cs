using System;
using Credit.DataTypes;

namespace Credit.Factories
{
    public static class InsuranceRateFactory
    {
        private const float ATHLETIC_MODIFIER = -0.05f;
        private const float SMOKER_MODIFIER = 0.15f;
        private const float HEART_DISEASE_MODIFIER = 0.3f;
        private const float IT_ENGINEER_MODIFIER = -0.05f;
        private const float FIGHTER_PILOT_MODIFIER = 0.15f;

        public static IInsuranceRate CreateMortgageInsuranceRate(BorrowerRisks pBorrowerRisks)
        {
            float insuranceRate = MortgageInsuranceRate.BASE_INSURANCE_RATE;

            if (pBorrowerRisks.IsAthletic)
                insuranceRate += ATHLETIC_MODIFIER;
            if (pBorrowerRisks.IsSmoker)
                insuranceRate += SMOKER_MODIFIER;
            if (pBorrowerRisks.HasHeartDisease)
                insuranceRate += HEART_DISEASE_MODIFIER;
            if (pBorrowerRisks.IsItEngineer)
                insuranceRate += IT_ENGINEER_MODIFIER;
            if (pBorrowerRisks.IsFighterPilot)
                insuranceRate += FIGHTER_PILOT_MODIFIER;

            insuranceRate = MathF.Round(insuranceRate, 2);
            return new MortgageInsuranceRate(insuranceRate);
        }
    }
}