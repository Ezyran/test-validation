namespace Credit
{
    public class BorrowerRisks
    {
        public bool IsAthletic { get; set; }
        public bool IsSmoker { get; set; }
        public bool HasHeartDisease { get; set; }
        public bool IsItEngineer { get; set; }
        public bool IsFighterPilot { get; set; }

        public BorrowerRisks(bool pIsAthletic, bool pIsSmoker, bool pHasHeartDisease, bool pIsItEngineer, bool pIsFighterPilot)
        {
            IsAthletic = pIsAthletic;
            IsSmoker = pIsSmoker;
            HasHeartDisease = pHasHeartDisease;
            IsFighterPilot = pIsFighterPilot;
            IsItEngineer = pIsItEngineer;
        }
    }
}