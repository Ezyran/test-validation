using System;
using Credit.DataTypes;
using Credit.Exceptions;

namespace Credit
{
    public class Loan
    {
        private IAmount _amount;
        private IDuration _duration;
        private IInterestRate _interestRate;
        private IInsuranceRate _insuranceRate;

        public Loan(IAmount pAmount, IDuration pDuration, IInterestRate pInterestRate, IInsuranceRate pInsuranceRate)
        {
            _amount = pAmount;
            _duration = pDuration;
            _interestRate = pInterestRate;
            _insuranceRate = pInsuranceRate;
        }

        public float GetMonthlyPayment()
        {
            return GetMonthlyReimbursment() + GetMonthlyInsurancePayment();
        }
        
        public float GetMonthlyReimbursment()
        {
            double interestRate = _interestRate.InterestRate / 100f;
            double monthlyCapital = _amount.Amount * (interestRate / 12);
            double denominator = 1 - Math.Pow((1 + interestRate / 12), (-_duration.DurationInYears * 12));
            return (float) (monthlyCapital / denominator);
        }

        public float GetMonthlyInsurancePayment()
        {
            float insuranceRate = _insuranceRate.InsuranceRate / 100f;
            return _amount.Amount * insuranceRate / 12;
        }

        public float GetTotalInterestCost()
        {
            return (GetMonthlyReimbursment() * _duration.DurationInYears * 12) - _amount.Amount;
        }

        public float GetTotalInsuranceCost()
        {
            return GetMonthlyInsurancePayment() * _duration.DurationInYears * 12;
        }

        public float GetReimbursedAmountAfterYears(uint pYears)
        {
            if (pYears < 1 || pYears > _duration.DurationInYears)
                throw new InvalidDurationException($"Years of reimbursment must be between 1 and ${_duration.DurationInYears}");
            return GetMonthlyReimbursment() * pYears * 12;
        }
    }
}