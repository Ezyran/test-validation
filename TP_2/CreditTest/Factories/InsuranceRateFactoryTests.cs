using System.Collections;
using System.Collections.Generic;
using Credit;
using Credit.DataTypes;
using Credit.Factories;
using Xunit;

namespace CreditTest.Factories
{
    public class InsuranceRateFactoryData
    {
        public static IEnumerable<object[]> Data
        {
            get
            {
                yield return new object[] {0.8f, new BorrowerRisks(true, true, true, true, true)};
                yield return new object[] {0.3f, new BorrowerRisks(false, false, false, false, false)};
                yield return new object[] {0.2f, new BorrowerRisks(true, false, false, true, false)};
            }
        }
    }
    
    public class InsuranceRateFactoryTests
    {
        [Theory]
        [MemberData(nameof(InsuranceRateFactoryData.Data), MemberType = typeof(InsuranceRateFactoryData))]
        public void GetGoodValues(float pExpectedValue, BorrowerRisks pBorrowerRisks)
        {
            var insuranceRate = InsuranceRateFactory.CreateMortgageInsuranceRate(pBorrowerRisks);
            Assert.Equal(insuranceRate.InsuranceRate, pExpectedValue);
        }
    }
}