using System.Collections.Generic;
using Credit;
using Credit.DataTypes;
using Credit.Enums;
using Credit.Factories;
using Xunit;

namespace CreditTest.Factories
{
    public class InterestRateFactoryData
    {
        public static IEnumerable<object[]> Data
        {
            get
            {
                yield return new object[] {0.67f, new MortgageDuration(10), ERateType.BonTaux};
                yield return new object[] {0.91f, new MortgageDuration(20), ERateType.TresBonTaux};
                yield return new object[] {0.58f, new MortgageDuration(15), ERateType.ExcellentTaux};
            }
        }
    }
    
    public class InterestRateFactoryTests
    {
        [Theory]
        [MemberData(nameof(InterestRateFactoryData.Data), MemberType = typeof(InterestRateFactoryData))]
        public void GetGoodValues(float pExpectedValue, MortgageDuration pDuration, ERateType pRateType)
        {
            var mortgageRate = InterestRateFactory.CreateMortgageInterestRate(pRateType, pDuration);
            Assert.Equal(mortgageRate.InterestRate, pExpectedValue);
        }
    }
}