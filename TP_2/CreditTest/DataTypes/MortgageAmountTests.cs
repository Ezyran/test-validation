using Credit.DataTypes;
using Credit.Exceptions;
using Xunit;

namespace CreditTest.DataTypes
{
    public class MortgageAmountTests
    {
        private const uint _amountTooLow = 49000;

        [Theory]
        [InlineData(_amountTooLow)]
        public void CantExceedMinValue(uint pAmountTooLow)
        {
            InvalidAmountException ex = Assert.Throws<InvalidAmountException>(() => new MortgageAmount(pAmountTooLow));
            Assert.Equal($"Given amount of {pAmountTooLow} is lower than minimal value of 50000", ex.Message);
        }
    }
}   