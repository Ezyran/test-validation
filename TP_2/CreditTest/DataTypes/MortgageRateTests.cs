using Credit.DataTypes;
using Credit.Exceptions;
using Xunit;

namespace CreditTest.DataTypes
{
    public class MortgageRateTests
    {
        private const float _minimumValue = 0f;
        private const float _maximumValue = 100f;

        [Theory]
        [InlineData(-0.1f)]
        public void CantExceedMinimumValue(float pValueTooLow)
        {
            InvalidInterestRateException ex = Assert.Throws<InvalidInterestRateException>(() => new MortgageRate(pValueTooLow));
            Assert.Equal($"Given interest rate of {pValueTooLow} is lesser than minimal value of {_minimumValue}", ex.Message);
        }

        [Theory]
        [InlineData(100.1f)]
        public void CantExceedMaximumValue(float pValueTooHigh)
        {
            InvalidInterestRateException ex = Assert.Throws<InvalidInterestRateException>(() => new MortgageRate(pValueTooHigh));
            Assert.Equal($"Given interest rate of {pValueTooHigh} is greater than maximal value of {_maximumValue}", ex.Message);
        }
    }
}