using Credit.DataTypes;
using Credit.Exceptions;
using Xunit;

namespace CreditTest.DataTypes
{
    public class MortgageDurationTests
    {
        private const uint _minimumDuration = 9;
        private const uint _maximumDuration = 25;

        [Theory]
        [InlineData(8)]
        public void CantExceedMinimumValue(uint pDurationTooShort)
        {
            InvalidDurationException ex = Assert.Throws<InvalidDurationException>(() => new MortgageDuration(pDurationTooShort));
            Assert.Equal($"Given duration of {pDurationTooShort} is lesser than minimal value of {_minimumDuration}", ex.Message);
        }
        
        [Theory]
        [InlineData(30)]
        public void CantExceedMaximumValue(uint pDurationTooLong)
        {
            InvalidDurationException ex = Assert.Throws<InvalidDurationException>(() => new MortgageDuration(pDurationTooLong));
            Assert.Equal($"Given duration of {pDurationTooLong} is greater than maximal value of {_maximumDuration}", ex.Message);
        }
    }
}