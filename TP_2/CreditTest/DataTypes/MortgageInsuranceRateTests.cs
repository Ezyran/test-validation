using Credit.DataTypes;
using Credit.Exceptions;
using Xunit;

namespace CreditTest.DataTypes
{
    public class MortgageInsuranceRateTests
    {
        private const float _minimumInsuranceRate = 0.10f;
        private const float _maximumInsuranceRate = 0.90f;

        [Theory]
        [InlineData(0.05f)]
        public void CantExceedMinimumValue(float pValueTooLow)
        {
            InvalidInsuranceRateException ex = Assert.Throws<InvalidInsuranceRateException>(() => new MortgageInsuranceRate(pValueTooLow));
            Assert.Equal($"Given Insurance Rate of {pValueTooLow} is lesser than the minimal value of {_minimumInsuranceRate}", ex.Message);
        }

        [Theory]
        [InlineData(1f)]
        public void CantExceedMaximumValue(float pValueTooHigh)
        {
            InvalidInsuranceRateException ex = Assert.Throws<InvalidInsuranceRateException>(() => new MortgageInsuranceRate(pValueTooHigh));
            Assert.Equal($"Given Insurance Rate of {pValueTooHigh} is greater than the maximal value of {_maximumInsuranceRate}", ex.Message);
        }
    }
}