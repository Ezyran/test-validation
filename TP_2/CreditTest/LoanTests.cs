using System.Collections.Generic;
using Credit;
using Credit.DataTypes;
using Credit.Enums;
using Credit.Factories;
using Xunit;

namespace CreditTest
{
    public class LoanData
    {
        public static IEnumerable<object[]> Data
        {
            get
            {
                var borrowerRisks = new BorrowerRisks(false, true, true, true, false);
                var amount = new MortgageAmount(175000);
                var duration = new MortgageDuration(25);
                var interestRate = InterestRateFactory.CreateMortgageInterestRate(ERateType.BonTaux, duration);
                var insurranceRate = InsuranceRateFactory.CreateMortgageInsuranceRate(borrowerRisks);
                var loan = new Loan(amount, duration, interestRate, insurranceRate);
                yield return new object[] {loan};
            }
        }
    }
    public class LoanTests
    {
        [Theory]
        [MemberData(nameof(LoanData.Data), MemberType = typeof(LoanData))]
        public void MonthlyPaymentTest(Loan pLoan)
        {
            Assert.Equal(783, pLoan.GetMonthlyPayment(), 0);
        }

        [Theory]
        [MemberData(nameof(LoanData.Data), MemberType = typeof(LoanData))]
        public void MonthlyInsurancePaymentTest(Loan pLoan)
        {
            Assert.Equal(102, pLoan.GetMonthlyInsurancePayment(), 0);
        }

        [Theory]
        [MemberData(nameof(LoanData.Data), MemberType = typeof(LoanData))]
        public void MonthlyReimbursment(Loan pLoan)
        {
            Assert.Equal(681, pLoan.GetMonthlyReimbursment(), 0);
        }
        
        [Theory]
        [MemberData(nameof(LoanData.Data), MemberType = typeof(LoanData))]
        public void TotalInterestCostTest(Loan pLoan)
        {
            Assert.Equal(29341, pLoan.GetTotalInterestCost(), 0);
        }
        
        [Theory]
        [MemberData(nameof(LoanData.Data), MemberType = typeof(LoanData))]
        public void TotalInsuranceCostTest(Loan pLoan)
        {
            Assert.Equal(30625, pLoan.GetTotalInsuranceCost(), 0);
        }
        
        [Theory]
        [MemberData(nameof(LoanData.Data), MemberType = typeof(LoanData))]
        public void ReimbursedAmountAfterYearsTest(Loan pLoan)
        {
            Assert.Equal(81736, pLoan.GetReimbursedAmountAfterYears(10), 0);
        }
    }
}