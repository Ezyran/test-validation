﻿using Credit;
using Credit.DataTypes;
using Credit.Enums;
using Credit.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CreditUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCalculer_Click(object sender, RoutedEventArgs e)
        {
            Loan loan = CreateLoanFromInput();
            DisplayLoanDetails(loan);
        }

        private Loan CreateLoanFromInput()
        {
            string rateTypeName = ((ComboBoxItem)lstRateTypes.SelectedItem).Content.ToString();
            ERateType rateType = ERateType.BonTaux;
            switch (rateTypeName)
            {
                case "Bon taux":
                    rateType = ERateType.BonTaux;
                    break;
                case "Très bon taux":
                    rateType = ERateType.TresBonTaux;
                    break;
                case "Excellent taux":
                    rateType = ERateType.ExcellentTaux;
                    break;
            }

            uint amountValue = UInt32.Parse(txtAmount.Text);
            uint durationValue = UInt32.Parse(txtDuration.Text);

            bool isSmoker = (ckbSmoker.IsChecked == true);
            bool isAhtletic = (ckbAthletic.IsChecked == true);
            bool hasHeartDisease = (ckbHeartDisease.IsChecked == true);
            bool isItEngineer = (ckbItEngineer.IsChecked == true);
            bool isFighterPilot = (ckbFighterPilot.IsChecked == true);

            BorrowerRisks borrowerRisks = new BorrowerRisks(isAhtletic, isSmoker, hasHeartDisease, isItEngineer, isFighterPilot);
            IAmount amount = new MortgageAmount(amountValue);
            IDuration duration = new MortgageDuration(durationValue);
            IInterestRate interestRate = InterestRateFactory.CreateMortgageInterestRate(rateType, duration);
            IInsuranceRate insuranceRate = InsuranceRateFactory.CreateMortgageInsuranceRate(borrowerRisks);

            return new Loan(amount, duration, interestRate, insuranceRate);
        }

        private void DisplayLoanDetails(Loan pLoan)
        {
            outputMonthlyCost.Content = Math.Round(pLoan.GetMonthlyPayment());
            outputMonthlyInsurance.Content = Math.Round(pLoan.GetMonthlyInsurancePayment());
            outputInterestTotalCost.Content = Math.Round(pLoan.GetTotalInterestCost());
            outputInsuranceTotalCost.Content = Math.Round(pLoan.GetTotalInsuranceCost());
            outputYearlyReimbursedAmount.Content = Math.Round(pLoan.GetReimbursedAmountAfterYears(1));
        }
    }
}
